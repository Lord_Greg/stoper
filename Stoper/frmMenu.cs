﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace Stoper
{
    public partial class frmMenu : Form
    {
        private static TimeSpan timeLeft, givenTime;
        private static Timer timer;
        private int timerInterval = 1000;

        public frmMenu()
        {
            InitializeComponent();

            timer = new Timer();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                givenTime = new TimeSpan(0, Convert.ToInt32(nudMinutes.Value), Convert.ToInt32(nudSeconds.Value));
                timeLeft = new TimeSpan(0, Convert.ToInt32(nudMinutes.Value), Convert.ToInt32(nudSeconds.Value));

                timerStart();
            }
            else
            {
                timerStop();
            }
        }


        private void timer_tick(Object sender, EventArgs e)
        {
            timeLeft = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(timeLeft.TotalMilliseconds) - timer.Interval);

            lblTime.Text = getTimeLeftS();
            
            if(timeLeft.TotalMilliseconds < 10)
            {
                timerStop();

                if (!chbMute.Checked)
                {
                    SystemSounds.Beep.Play();
                }

                MessageBox.Show("Czas Minął!");
            }
        }


        public void timerStart()
        {
            timer = new Timer();
            timer.Tick += new EventHandler(timer_tick);
            timer.Interval = timerInterval;

            timer.Start();

            lblTime.Text = getTimeLeftS();
            btnStart.Text = "Stop";
        }

        public void timerStop()
        {
            timer.Stop();
            btnStart.Text = "Start";
        }


        private string getTimeLeftS()
        {
            string timeLeftS = "";

            int minutes = Convert.ToInt32(Math.Floor(timeLeft.TotalMinutes));

            if (minutes < 10) timeLeftS += "0";
            timeLeftS += minutes.ToString();

            timeLeftS += ":";

            if (timeLeft.Seconds < 10) timeLeftS += "0";
            timeLeftS += timeLeft.Seconds.ToString();

            timeLeftS += ":";

            if (timeLeft.Milliseconds < 100) timeLeftS += "0";
            timeLeftS += (timeLeft.Milliseconds / 10).ToString();

            return timeLeftS;
        }



        public TimeSpan GivenTime
        {
            get { return givenTime; }
        }
    }
}
